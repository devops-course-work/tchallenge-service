FROM openjdk:8-jre

WORKDIR /app

COPY source/build/libs/tchallenge-service.jar .

ENTRYPOINT exec java -jar /app/tchallenge-service.jar
